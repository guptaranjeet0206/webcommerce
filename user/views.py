import io

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http.response import JsonResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt

# from .serializers import UserSerializer

# Create your views here.
from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser
from rest_framework.authtoken.views import ObtainAuthToken


@csrf_exempt
def register(request):
    if request.method == 'POST':
        req_data = request.body
        stream = io.BytesIO(req_data)
        data = JSONParser().parse(stream)

        if data:
            username = data.get('username')
            password = data.get('password')
            email = data.get('email')
            first_name = data.get('first_name')
            last_name = data.get('last_name')
            user = User.objects.create_user(username=username, password=password, email=email, first_name=first_name,
                                            last_name=last_name)
            if user:
                user.save()
                return JsonResponse(
                    {
                        "message": "User registered succefully",
                        "user": user.username
                    }
                )

            return JsonResponse(
                {
                    "message": "user not registered"
                }
            )


@csrf_exempt
def log_in(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user:
            token, created = Token.objects.get_or_create(user=user)
            return JsonResponse(
                {
                    "message": "Logged In",
                    "token": token.key
                }
            )
        else:
            return JsonResponse(
                {
                    "message": "check your username and password again"
                }
            )


def log_out(request):
    logout(request)
    return JsonResponse("SUCCESSFULLY LOGOUT", safe=False)
    # Redirect to a success page.
