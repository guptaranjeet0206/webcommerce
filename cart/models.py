from django.db import models
from product.models import products, categories
from django.contrib.auth.models import User


# Create your models here.
class cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(products, on_delete=models.CASCADE)
    price = models.IntegerField()
    quantity = models.IntegerField()
    total_amount = models.IntegerField()

    def __str__(self):
        return self.product.name
