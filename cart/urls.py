from django.urls import path
from cart import views

urlpatterns = [
    path('cart', views.view_cart.as_view())
]