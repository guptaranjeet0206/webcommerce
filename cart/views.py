import io

from django.shortcuts import render
from django.http.response import JsonResponse

from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from .models import cart
from product.models import products
from rest_framework import request


@method_decorator(csrf_exempt, name='dispatch')
class view_cart(APIView):

    """
    define three api, post-for adding product in cart, patch-for removing item if user has more than one same product
    delet - for deleting item from cart
    """

    permission_classes = [IsAuthenticated]

    def post(self, request):

        try:

            # user_id = request.POST.get('user_id')
            product_id = request.POST.get('product_id')
            product_stock = products.objects.get(id=product_id)
            if product_stock.stock > 0:

                # user_data = User.objects.get(id=user_id)
                user_data = request.user
                print(user_data)
                if user_data:
                    try:
                        product_found = cart.objects.filter(product_id=product_id)
                        print(product_found)
                    except Exception as e:
                        product_found = None
                    # print(product_found)
                    # print(product_found.user.id)

                    if product_found is not None:
                        for match in product_found:
                            if user_data.id == match.user.id:
                                # print(user_data.id, product_found.user.id)  # authentication
                                quantity = match.quantity
                                quantity += 1
                                total_amount = match.price * quantity
                                match.quantity = quantity
                                match.total_amount = total_amount
                                match.save()
                                return JsonResponse(
                                    {
                                        "message": "cart updated"
                                    }
                                )
                            else:
                                continue

                        quantity = 0
                        product_details = products.objects.get(id=product_id)
                        if product_details:
                            product = product_details
                            price = product_details.price
                            quantity += 1
                            total_amount = price * quantity

                            create_cart = cart(user=user_data, product=product, price=price, quantity=quantity,
                                               total_amount=total_amount)
                            create_cart.save()
                            return JsonResponse(
                                {
                                    "message": "product added to cart"
                                }
                            )

                        else:
                            return JsonResponse(
                                {
                                    "message": "sorry product is no longer available"
                                }
                            )

                    else:
                        quantity = 0
                        product_details = products.objects.get(id=product_id)
                        if product_details:
                            product = product_details
                            price = product_details.price
                            quantity += 1
                            total_amount = price * quantity

                            create_cart = cart(user=user_data, product=product, price=price, quantity=quantity,
                                               total_amount=total_amount)
                            create_cart.save()
                            return JsonResponse(
                                {
                                    "message": "product added to cart"
                                }
                            )

                        else:
                            return JsonResponse(
                                {
                                    "message": "sorry product is no longer available"
                                }
                            )

            else:
                return JsonResponse(
                    {
                        "message": "cant add product to the cart, product out of stock"
                    }
                )

        except Exception as e:
            return JsonResponse(
                {
                    "mesage": "somthing went wrong"
                }
            )

    def get(self, request):
        try:
            product_list = []
            cost = 0
            # user_id = request.GET.get('id')

            req_data = cart.objects.filter(user=request.user)

            if req_data:
                for val in req_data:
                    cost = cost + val.total_amount
                    product_list.append(
                        {
                            val.product.name: {
                                "quantity": val.quantity,
                                "price": val.price,
                                "total_amount": val.total_amount
                            }
                        }

                    )
                return JsonResponse(
                    {
                        "your products": product_list,
                        "you have to pay": cost
                    }, safe=False)

            else:
                return JsonResponse(
                    {
                        "message": "no product available with this user"
                    }
                )

        except Exception as e:
            return JsonResponse(
                {
                    "message": "invalid user"
                }
            )

    def patch(self, request):
        json_data = request.body
        stream = io.BytesIO(json_data)
        value = JSONParser().parse(stream)
        # user_id = value['user_id']
        product_id = value['product_id']
        try:
            req_data = cart.objects.get(user=request.user, product=product_id)
        except Exception as e:
            req_data = None
        if req_data:
            if req_data.quantity == 1:
                req_data.quantity -= 1
                req_data.save()
                if req_data.quantity == 0:
                    req_data.delete()
                    return JsonResponse(
                        {
                            "message": "item removed succefully"
                        }
                    )

                else:

                    req_data.delete()
                    return JsonResponse(
                        {
                            "message": "item removed succefully"
                        }
                    )

            elif req_data.quantity > 1:
                quantity = req_data.quantity
                quantity -= 1
                req_data.quantity = quantity
                req_data.total_amount = req_data.price * quantity
                req_data.save()
                return JsonResponse(
                    {
                        "message": "item removed succefully"
                    }
                )
            else:
                req_data.delete()
                return JsonResponse(
                    {
                        "message": "item removed succefully"
                    }
                )

    def delete(self, request):
        try:
            # user_id = request.GET.get('user_id')
            product_id = request.GET.get('product_id')
            req_data = cart.objects.get(user=request.user, product=product_id)
            if req_data:
                req_data.delete()
                return JsonResponse(
                    {
                        "message": "cart item deleted"
                    }
                )
            else:
                return JsonResponse(
                    {
                        "message": "product is not available in the cart"
                    }
                )
        except Exception as e:
            return JsonResponse(
                {
                    "message": "something went wrong"
                }
            )
