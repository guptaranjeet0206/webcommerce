from django.contrib import admin

# Register your models here.
from product.models import categories, products

admin.site.register(categories)
admin.site.register(products)
