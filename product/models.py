from django.db import models

# Create your models here.
from django.utils.datetime_safe import datetime


class categories(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class products(models.Model):
    category = models.ForeignKey(categories, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    price = models.IntegerField()
    image = models.ImageField(upload_to='Product/images', default="")
    stock = models.IntegerField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name
