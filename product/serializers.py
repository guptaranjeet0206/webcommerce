from .models import products, categories
from rest_framework import serializers


class categoriesSerializer(serializers.ModelSerializer):

    class Meta:
        model = categories
        fields = '__all__'


class productSerializer(serializers.ModelSerializer):

    class Meta:
        model = products
        fields = '__all__'

