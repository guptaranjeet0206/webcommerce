from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import authentication, permissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from .models import products, categories
from .serializers import productSerializer
from django.views import View


# Create your views here.
# @login_required()
@method_decorator(csrf_exempt, name='dispatch')
class product_details(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):

        try:
            name = request.GET.get('name')
            if name:
                data = products.objects.filter(name=name)
                if data:
                    serializer = productSerializer(data, many=True)
                    return JsonResponse(serializer.data, safe=False)
                else:
                    return JsonResponse(
                        {
                            "message": "NO PRODUCT WITH THIS NAME IS AVAILABLE"
                        }
                    )
            else:
                data = products.objects.all()
                serializer = productSerializer(data, many=True)
                if serializer:
                    return JsonResponse(serializer.data, safe=False)
                else:
                    return JsonResponse(
                        {
                            "message": "data not found"
                        }
                    )

        except Exception as e:
            return JsonResponse(
                {
                    "message": "something went wrong"
                }
            )
