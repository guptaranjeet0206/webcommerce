from django.urls import path
from product import views

urlpatterns = [
    path('product_details/', views.product_details.as_view())
]