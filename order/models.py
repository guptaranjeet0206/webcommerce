from datetime import datetime

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
from address.models import addresses


class order(models.Model):
    customer = models.ForeignKey(User, default='', on_delete=models.CASCADE)
    address = models.ForeignKey(addresses, default='', on_delete=models.CASCADE)
    product = models.CharField(max_length=200)
    pay_amount = models.IntegerField()
    date = models.DateTimeField(default=datetime.now)
    payment_id = models.BigIntegerField()

    def __str__(self):
        return self.pay_amount
