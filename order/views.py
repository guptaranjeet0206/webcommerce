import random

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated

from address.models import addresses
from cart.models import cart
from order.models import order


# @login_required()
@csrf_exempt
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def orders(request):
    try:
        cart_item = []
        pay_amount = 0
        if request.method == 'POST':
            print(request.user)
            # customer = request.POST.get('user_id')
            cust_data = User.objects.get(id=request.user.id)
            transaction_id = random.getrandbits(32)
            print(transaction_id)
            get_cart_item = cart.objects.filter(user=request.user.id)
            if get_cart_item:
                for val in get_cart_item:
                    pay_amount = pay_amount + val.total_amount
                    cart_item.append(
                        {
                            val.product.name: {
                                "quantity": val.quantity,
                                "price": val.price,
                                "total_amount": val.total_amount
                            }
                        }

                    )
            else:
                return JsonResponse(
                    {
                        "message": "cart is empty"
                    }
                )

            if transaction_id:
                customer_address = addresses.objects.get(user=request.user)
                if customer_address:
                    order_placed = order(customer=cust_data, address=customer_address, product=cart_item,
                                         pay_amount=pay_amount, payment_id=transaction_id)
                    order_placed.save()
                    return JsonResponse(
                        {
                            "message": "order placed successfully",
                            "payment_id": transaction_id,
                            "product": cart_item,
                            "Amount_paid": pay_amount,

                        }
                    )
                else:
                    return JsonResponse(
                        {
                            "message": "you need to provide your address first"
                        }
                    )

    except Exception as e:
        return JsonResponse(
            {
                "message": "something went wrong"
            }
        )
