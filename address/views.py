import io

from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from .models import addresses
from .serializers import AddressSerializer


# @login_required()
@method_decorator(csrf_exempt, name='dispatch')
class address_view(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        req_data = request.body
        stream = io.BytesIO(req_data)
        data = JSONParser().parse(stream)
        try:
            if data:
                serializer = AddressSerializer(data=data)
                if serializer.is_valid():
                    serializer.save()
                    return JsonResponse(
                        {
                            "message": "address saved succefully"
                        }
                    )
                else:
                    return JsonResponse(serializer.errors)

            else:
                return JsonResponse(
                    {
                        "message": "address not added"
                    }
                )
        except Exception as e:
            return JsonResponse(
                {
                    "message": "something went wrong"
                }
            )

    def get(self, request):
        try:
            req_data = addresses.objects.filter(user=request.user)
            if req_data:
                serializer = AddressSerializer(req_data, many=True)
                return JsonResponse(serializer.data, safe=False)


        except Exception as e:
            return JsonResponse(
                {
                    "message": "something went wrong...!"
                }
            )
