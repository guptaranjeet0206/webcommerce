from django.urls import path
from . import views

urlpatterns = [
    path('add_address/', views.address_view.as_view()),
    path('get_address/', views.address_view.as_view()),
]