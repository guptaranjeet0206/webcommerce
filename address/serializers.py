from rest_framework import serializers
from.models import addresses


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = addresses
        fields = '__all__'
