from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class addresses(models.Model):
    user = models.ForeignKey(User, default='', on_delete=models.CASCADE)
    state = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    landmark = models.CharField(max_length=200)
    pin_code = models.IntegerField()

    def __str__(self):
        return self.city
